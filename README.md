# portmididrv

Please note that portmididrv has moved to the main repository at gitlab.com/gomidi/midi/v2/drivers/portmididrv
All further development will be done there.
